exportToPDF() {
    this.loadingService.loadingOn();
    const doc = new jsPDF('landscape', 'mm', 'a3');

    const data = this.HostelRoomReportData;
    if (!Array.isArray(data) || data.length === 0) {
        console.error('No data available to generate the PDF.');
        this.loadingService.loadingOff();
        return;
    }

    const addressText = 'Address: ' + this.selectedSiteAddress();
    const logoUrl = this.selectedSiteLogoUrl();

    // Define fixed areas
    const headerHeight = 40; // Header height in mm
    const footerHeight = 20; // Footer height in mm
    const contentStartY = headerHeight + 5; // Start content just below the header
    const contentEndY = doc.internal.pageSize.height - footerHeight - 5; // End content just above the footer

    const tableColumn = [
        'Hostel',
        'Floor',
        'No of Rooms',
        'Room No',
        'Room Category',
        'Sl No',
        'Name',
        'Employee Code',
        'Room No',
        'Designation',
        'Place of Work',
        'Nationality',
    ];

    const tableRows: any[] = [];
    this.HostelRoomReportData.forEach((hostel: any) => {
        hostel.floor.forEach((floor: any) => {
            floor.room.forEach((room: any) => {
                room.request.forEach((request: any, index: number) => {
                    const row = [
                        hostel.hosteL_NAME,
                        floor.flooR_NAME,
                        room.introoM_COUNT,
                        room.rooM_NO,
                        room.roomcaT_NAME,
                        index + 1,
                        request.name,
                        request.employeE_CODE,
                        request.introoM_NO,
                        request.designation,
                        request.placE_OF_WORK,
                        request.nationality,
                    ];
                    tableRows.push(row);
                });
            });
        });
    });

    const convertImageToBase64 = (
        imgUrl: string,
        callback: (base64Image: string | null) => void
    ) => {
        const img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            const canvas = document.createElement('canvas');
            canvas.width = img.width;
            canvas.height = img.height;
            const ctx = canvas.getContext('2d');
            ctx?.drawImage(img, 0, 0);
            const dataURL = canvas.toDataURL('image/png');
            callback(dataURL);
        };
        img.onerror = function () {
            console.error('Failed to load the image.');
            callback(null);
        };
        img.src = imgUrl;
    };

    const addHeader = (doc: any, base64Image: string | null) => {
        if (base64Image) {
            doc.addImage(base64Image, 'PNG', 15, 10, 60, 20); // Logo
        }
        doc.setFontSize(12);
        doc.text(addressText, 15, 35); // Address text
    };

    const addFooter = (doc: any, pageNumber: number, totalPages: number) => {
        const pageWidth = doc.internal.pageSize.width;
        const footerY = doc.internal.pageSize.height - footerHeight + 10;

        doc.setDrawColor(160, 160, 160);
        doc.setLineWidth(0.05);
        doc.line(15, footerY - 5, pageWidth - 15, footerY - 5); // Footer separator line

        doc.setFontSize(10);
        const footerText = 'Powered by Shriconnect';
        const footerTextWidth = doc.getTextWidth(footerText);
        doc.text(footerText, pageWidth - footerTextWidth - 15, footerY);

        const date = new Date();
        const formattedDate = date.toLocaleDateString('en-GB', {
            year: 'numeric',
            month: 'short',
            day: 'numeric',
        });
        const formattedTime = date.toLocaleTimeString([], {
            hour: '2-digit',
            minute: '2-digit',
        });
        const printedText = `Printed on: ${formattedDate} ${formattedTime}`;
        const printedWidth = doc.getTextWidth(printedText);
        doc.text(printedText, (pageWidth - printedWidth) / 2, footerY);

        doc.text(`Page ${pageNumber} of ${totalPages}`, 15, footerY);
    };

    const generatePDFContent = (base64Image: string | null) => {
        autoTable(doc, {
            startY: contentStartY, // Start table below the header
            head: [tableColumn],
            body: tableRows,
            styles: {
                fontSize: 10,
                cellPadding: 1,
                lineWidth: 0.2,
                lineColor: [0, 0, 0],
                halign: 'center',
                valign: 'middle',
            },
            headStyles: {
                fillColor: '',
                fontSize: 12,
                textColor: [0, 0, 0],
                font: 'TimesNewRoman',
                fontStyle: 'bold',
            },
            bodyStyles: {
                fillColor: '',
                textColor: [0, 0, 0],
                font: 'TimesNewRoman',
            },
            alternateRowStyles: {
                fillColor: '',
            },
            margin: { top: contentStartY, bottom: footerHeight + 5 }, // Ensure table respects header and footer
            didDrawPage: function (data) {
                addHeader(doc, base64Image); // Draw header
                const totalPages = doc.internal.getNumberOfPages();
                addFooter(doc, data.pageNumber, totalPages); // Draw footer
            },
        });
        doc.save('Hostel Room Details Report.pdf');
        this.loadingService.loadingOff();
    };

    convertImageToBase64(logoUrl, generatePDFContent);
}

Property 'getNumberOfPages' does not exist on type '{ events: PubSub; scaleFactor: number; pageSize: { width: number; getWidth: () => number; height: number; getHeight: () => number; }; pages: number[]; getEncryptor(objectId: number): (data: string) => string; }'.

const titleText = 'Inmate Register Report';
      const titleWidth = doc.getTextWidth(titleText);
      const pageWidth = 420;
      const titleX = pageWidth - titleWidth - 15;
      const titleY = 25;
      doc.text(titleText, titleX, titleY);
      const dateoptions: Intl.DateTimeFormatOptions = {
        day: '2-digit',
        month: 'short',
        year: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        hour12: true,
      };
      const formattedDate = new Date().toLocaleString('en-GB', dateoptions);
      const dateText = `Date: ${formattedDate}`;
      const dateWidth = doc.getTextWidth(dateText);
      const dateX = pageWidth - dateWidth - 15;
      const dateY = 25;
      doc.text(dateText, dateX, dateY);