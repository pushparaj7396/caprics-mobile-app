USE [SCDB]
GO
/****** Object:  Table [dbo].[CampusMaster]    Script Date: 29-10-2024 23:08:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CampusMaster](
	[CM_ID] [int] IDENTITY(1,1) NOT NULL,
	[CM_SiteID] [int] NOT NULL,
	[CM_CampusName] [varchar](500) NOT NULL,
	[CM_CampusAddress] [varchar](500) NOT NULL,
	[CM_CreatedOn] [datetime] NOT NULL,
	[CM_CreatedBy] [int] NOT NULL,
	[CM_updatedOn] [datetime] NULL,
	[CM_updatedBy] [int] NULL,
 CONSTRAINT [CampusMaster_PK] PRIMARY KEY CLUSTERED 
(
	[CM_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[CampusMaster] ON 

INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (1, 47, N'Main Campus', N'Main Campus, ABC Site', CAST(N'2023-04-28T17:14:06.257' AS DateTime), 107970, NULL, NULL)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (2, 47, N'Primary', N'Primary Campus ABC Site', CAST(N'2023-04-28T17:14:31.357' AS DateTime), 107970, NULL, NULL)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (3, 74, N'Main School', N'Mesaimeer, Opposite Religious Complex, Abu Hamour', CAST(N'2023-05-05T03:31:07.090' AS DateTime), 108000, NULL, NULL)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (4, 74, N'Primary annex I', N'Jalba - Ibn - Umar, Zone No. 43, Doha', CAST(N'2023-05-05T03:33:13.593' AS DateTime), 108000, NULL, NULL)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (5, 74, N'Primary Annex II', N'Street No. 990, Sub-area No. 5, Zaid Bin Thabit, 43, Al Nuaija West', CAST(N'2023-05-05T03:33:31.507' AS DateTime), 108000, NULL, NULL)
SET IDENTITY_INSERT [dbo].[CampusMaster] OFF
GO

SET IDENTITY_INSERT [dbo].[CampusMaster] ON 

INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (1, 47, N'Main Campus', N'Main Campus, ABC Site', CAST(N'2023-04-28T17:14:06.257' AS DateTime), 107970, NULL, 0)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (2, 47, N'Primary', N'Primary Campus ABC Site', CAST(N'2023-04-28T17:14:31.357' AS DateTime), 107970, NULL, 0)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (3, 74, N'Main School', N'Mesaimeer, Opposite Religious Complex, Abu Hamour', CAST(N'2023-05-05T03:31:07.090' AS DateTime), 108000, NULL, 0)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (4, 74, N'Primary annex I', N'Jalba - Ibn - Umar, Zone No. 43, Doha', CAST(N'2023-05-05T03:33:13.593' AS DateTime), 108000, NULL, 0)
INSERT [dbo].[CampusMaster] ([CM_ID], [CM_SiteID], [CM_CampusName], [CM_CampusAddress], [CM_CreatedOn], [CM_CreatedBy], [CM_updatedOn], [CM_updatedBy]) VALUES (5, 74, N'Primary Annex II', N'Street No. 990, Sub-area No. 5, Zaid Bin Thabit, 43, Al Nuaija West', CAST(N'2023-05-05T03:33:31.507' AS DateTime), 108000, NULL, 0)
SET IDENTITY_INSERT [dbo].[CampusMaster] OFF
GO
